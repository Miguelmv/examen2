<?= $this->extend('layout/plantilla') ?>
<?php
$session = \Config\Services::session();
?>
<?= $this->section('content') ?>
<div class="alert alert-success">
    <?php print_r($session->solicitante)?>
</div>
<a href="<?= site_url('pauController/afegir') ?>" class="btn btn-primary">Afegir</a>
<table class="table table-striped table-condensed" id="myTable">
    <thead>
        <th>NIE/NIF</th>
        <th>Solicitante</th>
        <th>email</th>
        <th>ciclo</th>
        <th>matrícula</th>
        <th></th>
    </thead>
    <?php foreach ($solicitudes as $solicitud): ?>
        <tr>
            <td><?= $solicitud['nif'] ?></td>
            <td><?= $solicitud['solicitante'] ?></td>
            <td><?= $solicitud['email'] ?></td>
            <td><?= $solicitud['nombre'] ?></td>
            <td>
                <?= $solicitud['tipo_tasa']==1 ? 'ordinaria' : ($solicitud['tipo_tasa']==3 ? 'gratuita' : 'semigratuita') ?>
            </td>
            <td><a href="<?= site_url('pauController/carro/'.$solicitud['id'])?>"
            <td><a href="<?= site_url('pauController/borrar/'.$solicitud['id'])?>" 
                   class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar la solicitud de <?= $solicitud['solicitante'] ?>')">Borrar</a></td>
        </tr>
    <?php endforeach; ?>
</table>

<?= $this->endSection() ?>