<?= $this->extend('comun/layout') ?>
<?= $this->section('contenido') ?>
<?php $session = \Config\Services::session(); ?>
    <?php if ($session->has('carro')):?>
    <?php $carro = $session->get('carro');?>
    <div class="row"> 
        <table class="table table-striped">
        <?php foreach ($carro as $solicitud): ?>
        <tr>
            <td><?= $solicitud['nif'] ?></td>
            <td><?= $solicitud['solicitante'] ?></td>
            <td><?= $solicitud['email'] ?></td>
            <td><?= $solicitud['nombre'] ?></td>
        </tr>
        <?php endforeach; ?> 
        </table>
        <a href="<?= site_url('pauController/borrarCarro')?>" class="btn btn-danger">Vacía carro</a>
        <a href="<?= site_url('/')?>" class="btn btn-secondary">Seguir con las solicitudes</a>
    </div>                              
    <?php else : ?>
            <h3>No hay solicitudes</h3>
            <p>El carro está vacio</p>
    <?php endif ?>
<?= $this->endSection() ?>
                                                                      
<?= $this->section('titulo') ?>
<?= $titulo ?>
<?= $this->endSection() ?> 
