<?php namespace Config;

class IonAuth extends \IonAuth\Config\IonAuth
{
    // set your specific config
    public $siteTitle                = 'EXAMEN DE FEBRER 2022';       // Site Title, example.com
    // public $adminEmail               = 'admin@example.com'; // Admin Email, admin@example.com
    public $emailTemplates           = 'App\\Views\\auth\\email\\';
    // ...
}
